/*
	PSone Loader
	Copyright (C) 2016, TheFloW

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pspsdk.h>
#include <pspkernel.h>
#include <pspctrl.h>
#include <kubridge.h>

#include <stdio.h>
#include <string.h>
#include <stdarg.h>

PSP_MODULE_INFO("dumper", 0, 1, 0);

#define printf pspDebugScreenPrintf

int dumpBaseHeaders(char *path);

void errorExit(int milisecs, char *fmt, ...) {
	va_list list;
	char msg[256];	

	va_start(list, fmt);
	vsprintf(msg, fmt, list);
	va_end(list);

	printf(msg);

	sceKernelDelayThread(milisecs * 1000);
	sceKernelExitGame();
}

int main(int argc, char *argv[]) {
	pspDebugScreenInit();

	printf("PSone Loader: Dumper\n");
	printf("by TheFloW\n\n");
	printf("Press X to dump the base headers at ms0:/EBOOT.PBP.\n\n");

	while (1) {
		SceCtrlData pad;
		sceCtrlReadBufferPositive(&pad, 1);

		if (pad.Buttons & PSP_CTRL_CROSS)
			break;

		sceKernelDelayThread(50000);
	}

	printf("Loading required module...");
	sceKernelDelayThread(1 * 1000 * 1000);

	SceUID mod = kuKernelLoadModule("helper.prx", 0, NULL);
	if (mod < 0)
		errorExit(5000, "Error 0x%08X loading module.\n", mod);

	mod = sceKernelStartModule(mod, 0, NULL, NULL, NULL);
	if (mod < 0)
		errorExit(5000, "Error 0x%08X starting module.\n", mod);

	printf("OK\n");
	sceKernelDelayThread(1 * 1000 * 1000);

	printf("Dumping base headers...");
	sceKernelDelayThread(1 * 1000 * 1000);

	int res = dumpBaseHeaders("ms0:/EBOOT.PBP");
	if (res < 0)
		errorExit(5000, "Error 0x%08X dumping base headers.\n", res);

	printf("OK\n");
	sceKernelDelayThread(1 * 1000 * 1000);

	errorExit(5000, "Exiting...\n");

	return 0;
}