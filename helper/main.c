/*
	PSone Loader
	Copyright (C) 2016, TheFloW

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pspsdk.h>
#include <pspkernel.h>
#include <pspcrypt.h>

#include <stdio.h>
#include <string.h>

#include "main.h"
#include "utils.h"

PSP_MODULE_INFO("helper", 0x1006, 1, 0);

typedef struct {
	u8 psp_header[PSP_HEADER_SIZE];
	u8 kirk_header[KIRK_HEADER_SIZE];
} BaseHeaders;

static BaseHeaders base_headers;

// PGD decryption by Hykem
// https://github.com/Hykem/psxtract/blob/master/Linux/crypto.c

int getVersionKey(char *file, u8 *version_key) {
	// Open file
	SceUID fd = sceIoOpen(file, PSP_O_RDONLY, 0);
	if (fd < 0)
		return fd;

	// Read header
	PBPHeader header;
	sceIoRead(fd, &header, sizeof(PBPHeader));

	// Read PSP header
	sceIoLseek(fd, header.elf_offset, PSP_SEEK_SET);
	sceIoRead(fd, base_headers.psp_header, PSP_HEADER_SIZE);

	// Get magic
	char magic[16];
	sceIoLseek(fd, header.psar_offset, PSP_SEEK_SET);
	sceIoRead(fd, magic, sizeof(magic));

	if (memcmp(magic, "PSISOIMG0000", 12) == 0) { // Single-Disc
		sceIoLseek(fd, header.psar_offset + 0x400, PSP_SEEK_SET);
	} else if (memcmp(magic, "PSTITLEIMG000000", 16) == 0) { // Multi-Disc
		sceIoLseek(fd, header.psar_offset + 0x200, PSP_SEEK_SET);
	} else {
		return -1;
	}

	// Read PGD buffer
	static u8 pgd_buf[0x80];
	sceIoRead(fd, pgd_buf, sizeof(pgd_buf));

	// Close fd
	sceIoClose(fd);

	// Check PGD magic
	if (((u32 *)pgd_buf)[0] != PGD_MAGIC)
		return -2;

	// Set mac type
	int mac_type = 0;

	if (((u32 *)pgd_buf)[2] == 1) {
		mac_type = 1;

		if (((u32 *)pgd_buf)[1] > 1)
			mac_type = 3;
	} else {
		mac_type = 2;
	}

	// Generate the key from MAC 0x70 
	MAC_KEY mac_key;
	sceDrmBBMacInit(&mac_key, mac_type);
	sceDrmBBMacUpdate(&mac_key, pgd_buf, 0x70);

	u8 xor_keys[VERSION_KEY_SIZE];
	sceDrmBBMacFinal(&mac_key, xor_keys, NULL);

	u8 kirk_buf[VERSION_KEY_SIZE + KIRK7_HEADER_SIZE];

	if (mac_key.type == 3) {
		memcpy(kirk_buf + KIRK7_HEADER_SIZE, pgd_buf + 0x70, VERSION_KEY_SIZE);
		kirk7(kirk_buf, VERSION_KEY_SIZE, 0x63);
	} else {
		memcpy(kirk_buf, pgd_buf + 0x70, VERSION_KEY_SIZE);
	}

	memcpy(kirk_buf + KIRK7_HEADER_SIZE, kirk_buf, VERSION_KEY_SIZE);
	kirk7(kirk_buf, VERSION_KEY_SIZE, (mac_key.type == 2) ? 0x3A : 0x38);

	// Get version key
	int i;
	for (i = 0; i < VERSION_KEY_SIZE; i++) {
		version_key[i] = xor_keys[i] ^ kirk_buf[i];
	}

	return 0;
}

int sceUtilsBufferCopyWithRangePatched(void *inbuf, int insize, void *outbuf, int outsize, int cmd) {
	if (cmd == 1)
		memcpy(base_headers.kirk_header, outbuf, KIRK_HEADER_SIZE);

	return sceUtilsBufferCopyWithRange(inbuf, insize, outbuf, outsize, cmd);
}

int dumpBaseHeaders(char *path) {
	int k1 = pspSdkSetK1(0);

	// Find stub
	u32 stub = findModuleImport("sceMesgLed", "semaphore", 0x4C537C72);

	// Patch function to fetch the kirk header
	MAKE_JUMP(stub, sceUtilsBufferCopyWithRangePatched);
	clearCaches();

	// Register getId function
	sceKernelRegisterGetIdFunc(getVersionKey);

	// Load 'simple' module
	int res = sceKernelLoadModuleForLoadExecVSHMs5(PSP_INIT_APITYPE_MS5, path, 0, NULL);
	if (res >= 0) {
		// Write base headers
		WriteFile("ms0:/BASE_HEADERS.BIN", &base_headers, sizeof(BaseHeaders));
	}

	// Unregister getId function
	sceKernelRegisterGetIdFunc((void *)-1);

	// Restore
	MAKE_JUMP(stub, sceUtilsBufferCopyWithRange);
	clearCaches();

	pspSdkSetK1(k1);
	return res;
}

int module_start(SceSize args, void *argp) {
	return 0;
}