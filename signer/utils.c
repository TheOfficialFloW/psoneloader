/*
	PSone Loader
	Copyright (C) 2016, TheFloW

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <zlib.h>

#include "main.h"
#include "utils.h"

int ReadFile(char *file, void *buf, int size) {
	FILE *f = fopen(file, "rb");
	if (!f)
		return -1;

	int read = fread(buf, 1, size, f);

	fclose(f);
	return read;
}

int WriteFile(char *file, void *buf, int size) {
	FILE *f = fopen(file, "wb");
	if (!f)
		return -1;

	int written = fwrite(buf, 1, size, f);

	fclose(f);
	return written;
}

int gzipCompress(u8 *dst, const u8 *src, int size) {
	int ret;

	z_stream strm;
	strm.zalloc = Z_NULL;
	strm.zfree = Z_NULL;
	strm.opaque = Z_NULL;

	u8 *buffer = malloc(size);
	if (!buffer)
		return -1;

	ret = deflateInit2(&strm, 9, Z_DEFLATED, 15 + 16, 8, Z_DEFAULT_STRATEGY);
	if (ret != Z_OK) {
		free(buffer);
		return -2;
	}

	strm.avail_in = size;
	strm.next_in = (void *)src;
	strm.avail_out = size;
	strm.next_out = buffer;

	ret = deflate(&strm, Z_FINISH);
	if (ret == Z_STREAM_ERROR) {
		deflateEnd(&strm);
		free(buffer);
		return -3;
	}

	memcpy(dst, buffer, strm.total_out);

	deflateEnd(&strm);
	free(buffer);

	return strm.total_out;
}