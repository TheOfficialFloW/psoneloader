/*
	PSone Loader
	Copyright (C) 2016, TheFloW

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __MAIN_H__
#define __MAIN_H__

#define ALIGN(x, align) (((x) + ((align) - 1)) & ~((align) - 1))

#define PBP_MAGIC 0x50425000

#define CMAC_BLOCK_SIZE 0x90

#define PSP_HEADER_SIZE 0x960
#define KIRK_HEADER_SIZE 0x920

#define BIG_BUFFER_SIZE 16 * 1024 * 1024

typedef unsigned char u8;
typedef unsigned short int u16;
typedef unsigned int u32;

enum PBPEntries {
	PBP_ENTRY_PARAM_SF0,
	PBP_ENTRY_ICON0_PNG,
	PBP_ENTRY_ICON1_PMF,
	PBP_ENTRY_PIC0_PNG,
	PBP_ENTRY_PIC1_PNG,
	PBP_ENTRY_SND0_AT3,
	PBP_ENTRY_DATA_PSP,
	PBP_ENTRY_DATA_PSAR,
	N_PBP_ENTRIES,
};

typedef struct {
	u32 magic;
	u32 version;
	u32 offset[N_PBP_ENTRIES];
} PBPHeader;

typedef struct {
	u8 aes[16];
	u8 cmac[16];
} HeaderKeys;

#endif