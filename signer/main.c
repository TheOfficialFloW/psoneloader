/*
	PSone Loader
	Copyright (C) 2015, TheFloW

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <string.h>
#include <malloc.h>

#include "main.h"
#include "utils.h"

#include "libkirk/kirk_engine.h"
#include "libkirk/psp_headers.h"

#include "simple.h"

typedef struct {
	u8 psp_header[PSP_HEADER_SIZE];
	u8 kirk_header[KIRK_HEADER_SIZE];
} BaseHeaders;

static BaseHeaders base_headers;

static u8 *big_buffer = NULL;

int getKirkSize(void *buf) {
	return ALIGN(*(u32 *)(buf + 0x70), 0x10);
}

int isCompressed(void *buf) {
	return ((PSP_Header2 *)buf)->comp_attribute & 0x1;
}

int getElfSize(void *buf) {
	return ((PSP_Header2 *)buf)->elf_size;
}

int getPspSize(void *buf) {
	return ((PSP_Header2 *)buf)->psp_size;
}

int signSimpleModule(FILE *out, char *base_headers_file) {
	// Init kirk
	kirk_init();

	// Get base headers
	if (ReadFile(base_headers_file, &base_headers, sizeof(BaseHeaders)) != sizeof(BaseHeaders)) {
		printf("Invalid base headers file.\n");
		return -1;
	}

	// Compare content id
	if (strcmp((char *)base_headers.psp_header + 0x560, (char *)base_headers.kirk_header + 0x520) != 0) {
		printf("Invalid base headers.\n");
		return -1;
	}

	// Clean big buffer
	memset(big_buffer, 0, BIG_BUFFER_SIZE);

	// Copy kirk header
	memcpy(big_buffer, base_headers.kirk_header, KIRK_HEADER_SIZE);

	// Decrypt keys
	HeaderKeys keys;
	kirk_decrypt_keys((u8 *)&keys, big_buffer);
	memcpy(big_buffer, &keys, sizeof(HeaderKeys));

	// Copy simple module
	int simple_size = size_simple;
	memcpy((void *)big_buffer + KIRK_HEADER_SIZE, simple, simple_size);

	// Check uncompressed size
	if (simple_size > getElfSize(base_headers.psp_header)) {
		printf("Base base module is too small.\n");
		return -1;
	}

	// Compress
	if (isCompressed(base_headers.psp_header)) {
		simple_size = gzipCompress((void *)big_buffer + KIRK_HEADER_SIZE, (void *)big_buffer + KIRK_HEADER_SIZE, getElfSize(base_headers.psp_header));
		if (simple_size < 0) {
			printf("Could not compress base module.\n");
			return -1;
		}
	}

	// Check compressed size
	if (simple_size > getPspSize(base_headers.psp_header)) {
		printf("Base base module is too small.\n");
		return -1;
	}

	// Encrypt
	if (kirk_CMD0(big_buffer, big_buffer, sizeof(big_buffer)) != 0) {
		printf("Could not encrypt base module.\n");
		return -1;
	}

	// Forge cmac block
	memcpy(big_buffer, base_headers.kirk_header, CMAC_BLOCK_SIZE);
	if (kirk_forge(big_buffer, sizeof(big_buffer)) != 0) {
		printf("Could not forge cmac block.\n");
		return -1;
	}

	// Write fake signed DATA.PSP
	fwrite(base_headers.psp_header, 1, PSP_HEADER_SIZE, out);
	fwrite(big_buffer + KIRK_HEADER_SIZE, 1, getKirkSize(base_headers.kirk_header), out);

	return PSP_HEADER_SIZE + getKirkSize(base_headers.kirk_header);
}

u32 extractPbpEntry(FILE *in, FILE *out, u32 offset, u32 size) {
	fseek(in, offset, SEEK_SET);

	u32 seek = 0;
	while (seek < size) {
		u32 remain = size - seek;
		int read = fread(big_buffer, 1, (remain < BIG_BUFFER_SIZE) ? remain : BIG_BUFFER_SIZE, in);
		if (read < 0)
			return read;

		int written = fwrite(big_buffer, 1, read, out);
		if (written < 0)
			return written;

		seek += read;
	}

	return seek;
}

int sign(char *base_headers_file, char *input, char *output) {
	// Open input file
	FILE *in = fopen(input, "rb");
	if (!in) {
		printf("Could not open %s.\n", input);
		return -1;
	}

	// Open output file
	FILE *out = fopen(output, "wb");
	if (!out) {
		printf("Could not open %s.\n", output);
		return -1;
	}

	// Get input file size
	fseek(in, 0, SEEK_END);
	u32 file_size = ftell(in);
	fseek(in, 0, SEEK_SET);

	// Read PBP header
	PBPHeader header;
	fread(&header, 1, sizeof(PBPHeader), in);
	if (header.magic != PBP_MAGIC) {
		printf("Invalid PBP magic.\n");
		return -1;
	}

	// Write dummy PBP header
	fwrite(&header, 1, sizeof(PBPHeader), out);

	// Offsets
	u32 current_offset = sizeof(PBPHeader);

	int i;
	for (i = 0; i < N_PBP_ENTRIES; i++) {
		// Entry offset
		u32 offset = header.offset[i];

		// Update header offset
		header.offset[i] = current_offset;

		// Get entry size
		u32 size = 0;
		if (i == PBP_ENTRY_DATA_PSAR) {
			size = file_size - offset;
		} else {
			size = header.offset[i + 1] - offset;
		}

		// Write entry
		if (size > 0) {
			fseek(out, current_offset, SEEK_SET);

			if (i == PBP_ENTRY_DATA_PSP) {
				int res = signSimpleModule(out, base_headers_file);
				if (res < 0) {
					fclose(out);
					fclose(in);
					return res;
				}

				current_offset += res;
				current_offset = ALIGN(current_offset, 0x10000); // Align DATA.PSAR offset
			} else {
				int res = extractPbpEntry(in, out, offset, size);
				if (res < 0) {
					printf("Could not extract PBP entry.\n");
					fclose(out);
					fclose(in);
					return res;
				}

				current_offset += res;
			}
		}
	}

	// Update PBP header
	fseek(out, 0, SEEK_SET);
	fwrite(&header, 1, sizeof(PBPHeader), out);

	fclose(out);
	fclose(in);

	return 0;
}

int main(int argc, char *argv[]) {
	printf("PSone Loader: Signer\n");
	printf("by TheFloW\n\n");

	// Check arguments
	if (argc < 3 || argc > 4) {
		printf("Usage: %s <base headers> <input> <output>\n", argv[0]);
		return 0;
	}

	// Allocate big buffer
	big_buffer = malloc(BIG_BUFFER_SIZE);

	// Sign
	printf("Signing...");
	if (sign(argv[1], argv[2], "TEMP.PBP") == 0) {
		// Rename 'TEMP.PBP' to output
		char *output_file = (argc == 4) ? argv[3] : argv[2];
		remove(output_file);
		rename("TEMP.PBP", output_file);

		printf("Done.\n");
	}

	// Free big buffer
	free(big_buffer);

	return 0;
}