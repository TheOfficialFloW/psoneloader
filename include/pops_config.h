/*
	PSone Loader
	Copyright (C) 2016, TheFloW

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __POPS_CONFIG_H__
#define __POPS_CONFIG_H__

#define POPS_CONFIG_MAGIC_1 0x53504F50
#define POPS_CONFIG_MAGIC_2 0x4746432E

#define POPS_CONFIG_VERSION 5

enum PopsDiscLoadSpeeds {
	POPS_DISC_LOAD_SPEED_NORMAL,
	POPS_DISC_LOAD_SPEED_FAST,
};

enum PopsVolumeLevels {
	POPS_VOLUME_LEVEL_0,
	POPS_VOLUME_LEVEL_1,
	POPS_VOLUME_LEVEL_2,
};

enum PopsControllerNumbers {
	POPS_CTRL_NUMBER_1,
	POPS_CTRL_NUMBER_2,
};

enum PopsControllerModes {
	POPS_CTRL_MODE_DIGITAL,
	POPS_CTRL_MODE_ANALOG,
};

enum PopsButtonsAssignmentModes {
	POPS_BUTTONS_ASSIGNMENT_MODE_STANDARD,
	POPS_BUTTONS_ASSIGNMENT_MODE_CUSTOM,
};

enum PopsBilinearFiltering {
	POPS_NOT_USE_BILINEAR_FILTERING,
	POPS_USE_BILINEAR_FILTERING,
};

enum PopsScreenModes {
	POPS_SCREEN_MODE_ORIGINAL,
	POPS_SCREEN_MODE_NORMAL,
	POPS_SCREEN_MODE_ZOOM,
	POPS_SCREEN_MODE_FIT_SCREEN,
	POPS_SCREEN_MODE_CUSTOM,
};

enum PopsButtonAssignments {
	POPS_BUTTON_ASSIGN_UP,
	POPS_BUTTON_ASSIGN_DOWN,
	POPS_BUTTON_ASSIGN_LEFT,
	POPS_BUTTON_ASSIGN_RIGHT,
	POPS_BUTTON_ASSIGN_TRIANGLE,
	POPS_BUTTON_ASSIGN_CIRCLE,
	POPS_BUTTON_ASSIGN_CROSS,
	POPS_BUTTON_ASSIGN_SQUARE,
	POPS_BUTTON_ASSIGN_L,
	POPS_BUTTON_ASSIGN_R,
	POPS_BUTTON_ASSIGN_L_UP,
	POPS_BUTTON_ASSIGN_L_DOWN,
	POPS_BUTTON_ASSIGN_L_LEFT,
	POPS_BUTTON_ASSIGN_L_RIGHT,
	POPS_BUTTON_ASSIGN_R_UP,
	POPS_BUTTON_ASSIGN_R_DOWN,
	POPS_BUTTON_ASSIGN_R_LEFT,
	POPS_BUTTON_ASSIGN_R_RIGHT,
	POPS_BUTTON_ASSIGN_NUMBERS,
};

enum PopsButtonAssignmentValues {
	POPS_BUTTON_VALUE_UP,
	POPS_BUTTON_VALUE_DOWN,
	POPS_BUTTON_VALUE_LEFT,
	POPS_BUTTON_VALUE_RIGHT,
	POPS_BUTTON_VALUE_TRIANGLE,
	POPS_BUTTON_VALUE_CIRCLE,
	POPS_BUTTON_VALUE_CROSS,
	POPS_BUTTON_VALUE_SQUARE,
	POPS_BUTTON_VALUE_L1,
	POPS_BUTTON_VALUE_R1,
	POPS_BUTTON_VALUE_L2,
	POPS_BUTTON_VALUE_R2,
	POPS_BUTTON_VALUE_L3,
	POPS_BUTTON_VALUE_R3,
	POPS_BUTTON_VALUE_SELECT,
	POPS_BUTTON_VALUE_START,
	POPS_BUTTON_VALUE_NONE,
	POPS_BUTTON_VALUE_L1_R1,
	POPS_BUTTON_VALUE_L2_R2,
};

enum PopsTouchAssignments {
	POPS_TOUCH_ASSIGN_TOP_LEFT,
	POPS_TOUCH_ASSIGN_TOP_RIGHT,
	POPS_TOUCH_ASSIGN_BOTTOM_LEFT,
	POPS_TOUCH_ASSIGN_BOTTOM_RIGHT,
	POPS_TOUCH_ASSIGN_NUMBERS,
};

enum PopsTouchAssignmentValues {
	POPS_TOUCH_VALUE_NONE,
	POPS_TOUCH_VALUE_UP,
	POPS_TOUCH_VALUE_DOWN,
	POPS_TOUCH_VALUE_LEFT,
	POPS_TOUCH_VALUE_RIGHT,
	POPS_TOUCH_VALUE_TRIANGLE,
	POPS_TOUCH_VALUE_CIRCLE,
	POPS_TOUCH_VALUE_CROSS,
	POPS_TOUCH_VALUE_SQUARE,
	POPS_TOUCH_VALUE_SELECT,
	POPS_TOUCH_VALUE_START,
	POPS_TOUCH_VALUE_L1,
	POPS_TOUCH_VALUE_R1,
	POPS_TOUCH_VALUE_L2,
	POPS_TOUCH_VALUE_R2,
	POPS_TOUCH_VALUE_L3,
	POPS_TOUCH_VALUE_R3,
};

typedef struct {
	int x; // 0x00
	int y; // 0x04
	int width; // 0x08
	int height; // 0x0C
} PopsCustomScreen; // 0x10

// __sce_menuinfo structure
typedef struct {
	u32 magic_1; // 0x00
	u32 magic_2; // 0x04
	int version; // 0x08
	int unk; // 0x0C
	int disc_load_speed; // 0x10
	int volume_level; // 0x14
	int controller_number; // 0x18
	u16 controller_mode; // 0x1C
	u16 buttons_assignment_mode; // 0x1E
	int use_bilinear_filtering; // 0x20
	int screen_mode; // 0x24
	PopsCustomScreen custom_screen; // 0x28 
	u8 button_assignment[POPS_BUTTON_ASSIGN_NUMBERS]; // 0x38
	u8 front_touch_assignment[POPS_TOUCH_ASSIGN_NUMBERS]; // 0x4A
	u8 back_touch_assignment[POPS_TOUCH_ASSIGN_NUMBERS]; // 0x4E
} PopsConfiguration; // 0x400

#endif