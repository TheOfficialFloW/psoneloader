/*
	PSone Loader
	Copyright (C) 2016, TheFloW

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pspsdk.h>
#include <pspkernel.h>
#include <pspcrypt.h>

#include <stdio.h>
#include <string.h>

#include "main.h"
#include "utils.h"

void clearCaches() {
	void (* sceKernelDcacheWritebackInvalidateAll)() = (void *)0x88000744;
	void (* sceKernelIcacheInvalidateAll)() = (void *)0x88000E98;

	sceKernelDcacheWritebackInvalidateAll();
	sceKernelIcacheInvalidateAll();
}

void logmsg(char *msg) {
	SceUID fd = sceIoOpen("ms0:/PSP/SAVEDATA/PSVX00000/LOG.TXT", PSP_O_WRONLY | PSP_O_CREAT | PSP_O_APPEND, 0777);
	if (fd >= 0) {
		sceIoWrite(fd, msg, strlen(msg));
		sceIoClose(fd);
	}
}

int ReadFile(char *file, void *buf, int size) {
	SceUID fd = sceIoOpen(file, PSP_O_RDONLY, 0);
	if (fd < 0)
		return fd;

	int read = sceIoRead(fd, buf, size);

	sceIoClose(fd);
	return read;
}

int WriteFile(char *file, void *buf, int size) {
	SceUID fd = sceIoOpen(file, PSP_O_WRONLY | PSP_O_CREAT | PSP_O_TRUNC, 0777);
	if (fd < 0)
		return fd;

	int written = sceIoWrite(fd, buf, size);

	sceIoClose(fd);
	return written;
}

int _strcmp(const char *s1, const char *s2) {
	int val = 0;
	const unsigned char *u1, *u2;

	u1 = (unsigned char *) s1;
	u2 = (unsigned char *) s2;

	while (1) {
		if (*u1 != *u2) {
			val = (int) *u1 - (int) *u2;
			break;
		}

		if ((*u1 == 0) && (*u2 == 0)) {
			break;
		}

		u1++;
		u2++;
	}

	return val;
}

u32 findModuleImport(char *modname, char *libname, u32 nid) {
	SceModule2 *mod = sceKernelFindModuleByName(modname);
	if(!mod)
		return 0;

	int i = 0;
	while (i < mod->stub_size) {
		SceLibraryStubTable *stub = (SceLibraryStubTable *)(mod->stub_top + i);

		if (stub->libname && _strcmp(stub->libname, libname) == 0) {
			u32 *table = stub->nidtable;

			int j;
			for (j = 0; j < stub->stubcount; j++) {
				if (table[j] == nid) {
					return ((u32)stub->stubtable + (j * 8));
				}
			}
		}

		i += (stub->len * 4);
	}

	return 0;
}

u32 findModuleExport(char *modname, char *libname, u32 nid) {
	SceModule2 *mod = sceKernelFindModuleByName(modname);
	if (!mod)
		return 0;

	int i = 0;
	while (i < mod->ent_size) {
		SceLibraryEntryTable *entry = (SceLibraryEntryTable *)(mod->ent_top + i);

        if (entry->libname && _strcmp(entry->libname, libname) == 0) {
			u32 *table = entry->entrytable;
			int total = entry->stubcount + entry->vstubcount;

			int j;
			for (j = 0; j < total; j++) {
				if (table[j] == nid) {
					return table[j + total];
				}
			}
		}

		i += (entry->len * 4);
	}

	return 0;
}

void patchSyscall(u32 addr, void *newaddr) {
	void *ptr;
	asm("cfc0 %0, $12\n" : "=r"(ptr));

	u32 *syscalls = (u32 *)(ptr + 0x10);

	int i;
	for (i = 0; i < 0x1000; i++) {
		if ((syscalls[i] & 0x0FFFFFFF) == (addr & 0x0FFFFFFF)) {
			syscalls[i] = (u32)newaddr;
		}
	}
}

int kirk7(u8 *buf, int size, int type) {
	u32 *header = (u32 *)buf;

	header[0] = 5;
	header[1] = 0;
	header[2] = 0;
	header[3] = type;
	header[4] = size;

	return sceUtilsBufferCopyWithRange(buf, size + KIRK7_HEADER_SIZE, buf, size, 7);
}