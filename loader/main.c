/*
	PSone Loader
	Copyright (C) 2016, TheFloW

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pspsdk.h>
#include <pspkernel.h>
#include <pspcrypt.h>
#include <psputilsforkernel.h>

#include <stdio.h>
#include <string.h>

#include "main.h"
#include "pops_patch.h"
#include "utils.h"

typedef struct {
	void *function;
	char *modname;
	char *libname;
	u32 nid;
} Stub;

static Stub stubs[] = {
	// sceAmctrl_driver
	{ &sceDrmBBMacInit, "sceAmctrl_driver", "sceAmctrl_driver", 0x525B8218 },
	{ &sceDrmBBMacUpdate, "sceAmctrl_driver", "sceAmctrl_driver", 0x58163FBE },
	{ &sceDrmBBMacFinal, "sceAmctrl_driver", "sceAmctrl_driver", 0xEF95A213 },

	// IoFileMgrForKernel
	{ &sceIoOpen, "sceIOFileManager", "IoFileMgrForKernel", 0x109F50BC },
	{ &sceIoLseek, "sceIOFileManager", "IoFileMgrForKernel", 0x27EB27B8 },
	{ &sceIoWrite, "sceIOFileManager", "IoFileMgrForKernel", 0x42EC03AC },
	{ &sceIoIoctl, "sceIOFileManager", "IoFileMgrForKernel", 0x63632449 },
	{ &sceIoRead, "sceIOFileManager", "IoFileMgrForKernel", 0x6A638D83 },
	{ &sceIoRename, "sceIOFileManager", "IoFileMgrForKernel", 0x779103A0 },
	{ &sceIoClose, "sceIOFileManager", "IoFileMgrForKernel", 0x810C4BC3 },

	// LoadCoreForKernel
	{ &sceKernelFindModuleByUID, "sceLoaderCore", "LoadCoreForKernel", 0x40972E6E },

	// semaphore
	{ &sceUtilsBufferCopyWithRange, "sceMemlmd", "semaphore", 0x4C537C72 },

	// ModuleMgrForKernel
	{ &sceKernelLoadModule, "sceModuleManager", "ModuleMgrForKernel", 0x939E4270 },

	// scePopsMan
	{ &scePopsManLoadModule, "scePops_Manager", "scePopsMan", 0x29B3FB24 },

	// SysclibForKernel
	{ &memset, "sceSystemMemoryManager", "SysclibForKernel", 0x10F3BB61 },
	{ &strlen, "sceSystemMemoryManager", "SysclibForKernel", 0x52DF196C },
	{ &sprintf, "sceSystemMemoryManager", "SysclibForKernel", 0x7661E728 },
	{ &memcmp, "sceSystemMemoryManager", "SysclibForKernel", 0x81D0D1F7 },
	{ &memcpy, "sceSystemMemoryManager", "SysclibForKernel", 0xAB7592FF },
	{ &strcpy, "sceSystemMemoryManager", "SysclibForKernel", 0xEC6F1CF2 },

	// SysMemUserForUser
	{ &sceKernelSetCompiledSdkVersion, "sceSystemMemoryManager", "SysMemUserForUser", 0x358CA1BB },
	{ &sceKernelSetCompilerVersion, "sceSystemMemoryManager", "SysMemUserForUser", 0xF77D77CB },

	// UtilsForKernel
	{ &sceKernelDeflateDecompress, "sceSystemMemoryManager", "UtilsForKernel", 0xE8DB3CE6 },

	// ThreadManForKernel
	{ &sceKernelExitDeleteThread, "sceThreadManager", "ThreadManForKernel", 0x809CE29B },
};

void resolveStubs() {
	int i;
	for (i = 0; i < (sizeof(stubs) / sizeof(Stub)); i++) {
		u32 f = findModuleExport(stubs[i].modname, stubs[i].libname, stubs[i].nid);
		if (!f) {
			asm("break\n");
			while (1);
		}

		REDIRECT_FUNCTION((u32)stubs[i].function, f);
	}

	clearCaches();
}

int findImportantFunction() {
	u32 i;
	for (i = 0x88000000; i < (0x88400000 - 0x54 - 4); i += 4) {
		if (_lw(i + 0x00) == 0x27BDFFE0 && _lw(i + 0x04) == 0xAFB40010 &&
			_lw(i + 0x08) == 0xAFB3000C && _lw(i + 0x0C) == 0xAFB20008 &&
			_lw(i + 0x10) == 0x00009021 && _lw(i + 0x14) == 0x02409821 &&
			_lw(i + 0x54) == 0x0263202A)
		{
			REDIRECT_FUNCTION((u32)&sceKernelFindModuleByName, i);
			clearCaches();
			return 1;
		}
	}

	return 0;
}

__attribute__((section(".text.start"))) int _start(char *file) {
	// Find important function
	findImportantFunction();

	// Resolve stubs
	resolveStubs();

	// Init game
	initGame(file);

	// Patch scePops_Manager
	PatchPopsMan();

	// Set key
	getVersionKeyContentIdPatched(NULL, NULL, NULL);

	// Set versions
	sceKernelSetCompiledSdkVersion(0x06060110);
	sceKernelSetCompilerVersion(0x00030306);

	// Launch game
	scePopsManLoadModule(file, 0);

	return sceKernelExitDeleteThread(0);
}