/*
	PSone Loader
	Copyright (C) 2016, TheFloW

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __UTILS__H__
#define __UTILS__H__

#define KIRK7_HEADER_SIZE 0x14

#define log(...) \
{ \
	char msg[256]; \
	sprintf(msg,__VA_ARGS__); \
	logmsg(msg); \
}

void clearCaches();

void logmsg(char *msg);
int ReadFile(char *file, void *buf, int size);
int WriteFile(char *file, void *buf, int size);

u32 findModuleImport(char *modname, char *libname, u32 nid);
u32 findModuleExport(char *modname, char *libname, u32 nid);

void patchSyscall(u32 addr, void *newaddr);

int kirk7(u8 *buf, int size, int type);

#endif