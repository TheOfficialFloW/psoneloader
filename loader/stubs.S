	.set noreorder
	.set noat

.macro STUB name
.global \name
.ent \name
.type \name, %function
\name:
	jr $ra
	nop
.end \name
.endm

# sceAmctrl_driver
STUB sceDrmBBMacInit
STUB sceDrmBBMacUpdate
STUB sceDrmBBMacFinal

# IoFileMgrForKernel
STUB sceIoOpen
STUB sceIoLseek
STUB sceIoWrite
STUB sceIoIoctl
STUB sceIoRead
STUB sceIoRename
STUB sceIoClose

# LoadCoreForKernel
STUB sceKernelFindModuleByUID
STUB sceKernelFindModuleByName

# semaphore
STUB sceUtilsBufferCopyWithRange

# ModuleMgrForKernel
STUB sceKernelLoadModule

# scePopsMan
STUB scePopsManLoadModule

# SysclibForKernel
STUB memset
STUB strlen
STUB sprintf
STUB memcmp
STUB memcpy
STUB strcpy

# SysMemUserForUser
STUB sceKernelSetCompiledSdkVersion
STUB sceKernelSetCompilerVersion

# UtilsForKernel
STUB sceKernelDeflateDecompress

# ThreadManForKernel
STUB sceKernelExitDeleteThread