/*
	PSone Loader
	Copyright (C) 2016, TheFloW

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __MAIN_H__
#define __MAIN_H__

#define PSONELOADER_VERSION_MAJOR 1
#define PSONELOADER_VERSION_MINOR 0

#define REDIRECT_FUNCTION(a, f) \
{ \
	u32 func = a; \
	_sw(0x08000000 | (((u32)(f) >> 2) & 0x03FFFFFF), func); \
	_sw(0, func + 4); \
}

#define MAKE_DUMMY_FUNCTION(a, r) \
{ \
	u32 func = a; \
	if(r == 0) \
	{ \
		_sw(0x03E00008, func); \
		_sw(0x00001021, func + 4); \
	} \
	else \
	{ \
		_sw(0x03E00008, func); \
		_sw(0x24020000 | r, func + 4); \
	} \
}

#define K_EXTRACT_CALL(x) (((((u32)_lw((u32)x)) & ~0x0C000000) << 2) | 0x80000000)

#define PBP_MAGIC 0x50425000
#define ELF_MAGIC 0x464C457F
#define PSP_MAGIC 0x5053507E
#define PGD_MAGIC 0x44475000

#define PSP_O_GAMEDATA 0x40000000

typedef struct {
	u32 magic;
	u32 version;
	u32 param_offset;
	u32 icon0_offset;
	u32 icon1_offset;
	u32 pic0_offset;
	u32 pic1_offset;
	u32 snd0_offset;
	u32 elf_offset;
	u32 psar_offset;
} PBPHeader;

int scePopsManLoadModule(char *path, int mode);

#endif