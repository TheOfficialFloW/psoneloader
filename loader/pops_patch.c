/*
	PSone Loader
	Copyright (C) 2016, TheFloW

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pspsdk.h>
#include <pspkernel.h>
#include <psputilsforkernel.h>

#include <stdio.h>
#include <string.h>

#include "main.h"
#include "pops_patch.h"
#include "utils.h"

static int (* scePopsManExitVSHKernel)(int error);
static int (* setVersionKeyContentId)(char *file, u8 *version_key, char *content_id);

static u8 pgd_buf[0x80];
static int original = 0;

int scePopsManExitVSHKernelPatched(u32 destSize, u8 *src, u8 *dest) {
	if (destSize & 0x80000000)
		return scePopsManExitVSHKernel(destSize);

	int size = sceKernelDeflateDecompress(dest, destSize, src, 0);
	return (size == 0x9300) ? 0x92FF : size;
}

SceUID sceIoOpenPatched(const char *file, int flags, SceMode mode) {
	// Remove drm flag
	return sceIoOpen(file, flags & ~PSP_O_GAMEDATA, mode);
}

int sceIoIoctlPatched(SceUID fd, unsigned int cmd, void *indata, int inlen, void *outdata, int outlen) {
	// Seek
	if (cmd == 0x04100002)
		sceIoLseek(fd, *(u32 *)indata, PSP_SEEK_SET);

	return 0;
}

int sceIoReadPatched(SceUID fd, void *data, SceSize size) {
	int res = sceIoRead(fd, data, size);

	// Fake ~PSP magic to avoid crash
	if (size == sizeof(u32)) {
		u32 magic = ELF_MAGIC;
		if (memcmp(data, &magic, sizeof(u32)) == 0) {
			magic = PSP_MAGIC;
			memcpy(data, &magic, sizeof(u32));
		}
	}

	return res;
}

// PGD decryption by Hykem
// https://github.com/Hykem/psxtract/blob/master/Linux/crypto.c

int getVersionKeyContentIdPatched(char *file, u8 *version_key, char *content_id) {
	u8 dummy_version_key[VERSION_KEY_SIZE];
	char dummy_content_id[CONTENT_ID_SIZE];

	if (!version_key)
		version_key = dummy_version_key;

	if (!content_id)
		content_id = dummy_content_id;

	memset(version_key, 0, VERSION_KEY_SIZE);
	memset(content_id, 0, CONTENT_ID_SIZE);

	if (original) {
		// Set mac type
		int mac_type = 0;

		if (((u32 *)pgd_buf)[2] == 1) {
			mac_type = 1;

			if (((u32 *)pgd_buf)[1] > 1)
				mac_type = 3;
		} else {
			mac_type = 2;
		}

		// Generate the key from MAC 0x70 
		MAC_KEY mac_key;
		sceDrmBBMacInit(&mac_key, mac_type);
		sceDrmBBMacUpdate(&mac_key, pgd_buf, 0x70);

		u8 xor_keys[VERSION_KEY_SIZE];
		sceDrmBBMacFinal(&mac_key, xor_keys, NULL);

		u8 kirk_buf[VERSION_KEY_SIZE + KIRK7_HEADER_SIZE];

		if (mac_key.type == 3) {
			memcpy(kirk_buf + KIRK7_HEADER_SIZE, pgd_buf + 0x70, VERSION_KEY_SIZE);
			kirk7(kirk_buf, VERSION_KEY_SIZE, 0x63);
		} else {
			memcpy(kirk_buf, pgd_buf + 0x70, VERSION_KEY_SIZE);
		}

		memcpy(kirk_buf + KIRK7_HEADER_SIZE, kirk_buf, VERSION_KEY_SIZE);
		kirk7(kirk_buf, VERSION_KEY_SIZE, (mac_key.type == 2) ? 0x3A : 0x38);

		// Get version key
		int i;
		for (i = 0; i < VERSION_KEY_SIZE; i++) {
			version_key[i] = xor_keys[i] ^ kirk_buf[i];
		}
	}

	return setVersionKeyContentId(file, version_key, content_id);
}

void PatchPops(SceModule2 *mod) {
	u32 opcode = 0;

	int i;
	for (i = 0; i < mod->text_size; i += 4) {
		u32 addr = mod->text_addr + i;

		if (!original) {
			// Use our decompression function
			if (_lw(addr) == 0x36040005) {
				opcode = _lw(addr - 4);
				continue;
			}

			if (opcode) {
				if (_lw(addr) == 0x34049300) {
					_sw(opcode, addr - 4);
					continue;
				}
			}

			// Fix index length. This enables CDDA support
			if (_lw(addr) == 0x14C00014 && _lw(addr + 4) == 0x24E2FFFF) {
				_sh(0x1000, addr + 2);
				continue;
			}
		}
	}

	clearCaches();
}

SceUID sceKernelLoadModulePatched(const char *path, int flags, SceKernelLMOption *option) {
	SceUID modid = sceKernelLoadModule(path, flags, option);

	if (modid >= 0)
		PatchPops(sceKernelFindModuleByUID(modid));

	return modid;
}

void PatchPopsMan() {
	SceModule2 *mod = sceKernelFindModuleByName("scePops_Manager");
	u32 text_addr = mod->text_addr;

	if (!original) {
		// Patch syscall to use it as deflate decompress
		scePopsManExitVSHKernel = (void *)findModuleExport(mod->modname, "scePopsMan", 0x0090B2C8);
		patchSyscall((u32)scePopsManExitVSHKernel, scePopsManExitVSHKernelPatched);

		// Fake dnas drm
		REDIRECT_FUNCTION(findModuleImport(mod->modname, "IoFileMgrForKernel", 0x109F50BC), sceIoOpenPatched);
		REDIRECT_FUNCTION(findModuleImport(mod->modname, "IoFileMgrForKernel", 0x63632449), sceIoIoctlPatched);
		REDIRECT_FUNCTION(findModuleImport(mod->modname, "IoFileMgrForKernel", 0x6A638D83), sceIoReadPatched);

		// Dummying amctrl decryption functions
		MAKE_DUMMY_FUNCTION(text_addr + 0xA90, 1);
		_sw(0, text_addr + 0x53C);
	}

	// Patch this function to have control over pops module
	REDIRECT_FUNCTION(findModuleImport(mod->modname, "ModuleMgrForKernel", 0x939E4270), sceKernelLoadModulePatched);

	// Ignore path comparison. When launching with scePopsManLoadModule, it does check whether the path is equal the one at init. 
	_sh(0x1000, text_addr + 0x26);

	// Ignore version key and content id init check
	_sw(0, text_addr + 0x12C);

	// Patch key function. With this, KEYS.BIN or license files are not required anymore. Also this gives support to custom PSone games
	setVersionKeyContentId = (void *)text_addr + 0x124;
	REDIRECT_FUNCTION(K_EXTRACT_CALL(text_addr + 0xC64), getVersionKeyContentIdPatched);

	clearCaches();
}

int initGame(char *file) {
	// Open file
	SceUID fd = sceIoOpen(file, PSP_O_RDONLY, 0);
	if (fd < 0)
		return fd;

	// Read header
	PBPHeader header;
	sceIoRead(fd, &header, sizeof(PBPHeader));

	// Get magic
	char magic[16];
	sceIoLseek(fd, header.psar_offset, PSP_SEEK_SET);
	sceIoRead(fd, magic, sizeof(magic));

	if (memcmp(magic, "PSISOIMG0000", 12) == 0) { // Single-Disc
		sceIoLseek(fd, header.psar_offset + 0x400, PSP_SEEK_SET);
	} else if (memcmp(magic, "PSTITLEIMG000000", 16) == 0) { // Multi-Disc
		sceIoLseek(fd, header.psar_offset + 0x200, PSP_SEEK_SET);
	} else {
		return -1;
	}

	// Read PGD buffer
	sceIoRead(fd, pgd_buf, sizeof(pgd_buf));

	// Close fd
	sceIoClose(fd);

	// Check PGD magic
	if (((u32 *)pgd_buf)[0] == PGD_MAGIC)
		original = 1;

	return 0;
}