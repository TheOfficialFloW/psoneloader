# PSone Loader #

PSone Loader is a software which allows you to run PSone classic games from the PS Vita Livearea (custom PSone bubbles). It does support custom as well as official PSone games.
This software is made of 3 components:
The DUMPER, which does dump the PSP and KIRK headers of the 'simple' module (DATA.PSP) that is required for the SIGNER program.
The SIGNER, which does inject the fake signed custom 'simple' module into a PSone game converted as 'EBOOT.PBP'.
The LOADER, which does patch the pops module in order to allow custom converted PSone games and to bypass license verification of offical PSone games.

### Installation ###
Refer to the 'installation.txt' file.

### Notes ###
The method of custom PSone bubbles does not have the 'Settings' menu which is used to configure screen mode, button assignments, disc change, etc.
However the structure of the configuration file '__sce_menuinfo' has been documented at 'include/pops_config.h'. Therefore it is possible to write a PC app
that can do the configuration for your PSone games.

### Credits ###
* Major Tom and mr.gas for custom bubble rebirth method
* bbtgp for PrxEncrypter
* jigsaw for heap-overflow kernel exploit
* Hykem for PGD decryption algorithm
* vonjack for '__sce_menuinfo' research
* Dark_AleX for introducing custom converted PSone games.