	.set noreorder
	.set noat

.macro STUB name
.global \name
.ent \name
.type \name, %function
\name:
	jr $ra
	nop
.end \name
.endm

# LoadCoreForKernel
STUB sceKernelFindModuleByName